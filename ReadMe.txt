---------------------------------------------------------------------
CleaThin - Clean and Minimal Site Template
Based on: Bootstrap 3.3.6

Author: Valery Timofeev
Email:  spprofart@gmail.com

Thank you for purchasing my theme!
Made with Love ;-)
---------------------------------------------------------------------

Description
    CleaThin is a fully responsive site template based on Twitter Bootstrap.

Specification
    Clean Design
    CSS Animation
    Simple install
    Easy Customizable
    Bootstrap 3.x
    800+ Free Icons
    Free Google Font
    Fully Responsive Design
    Working PHP Contact Form
    12 Business Pages
    10 Color Presets
    3 Navigation Styles
    2 Navigation Positions

Credits
    Raleway Google Font
    Font Awesome Icons
    Elegant Icons
    Bootstrap 3.3.6
    OWL Carousel
    Magnific PopUp Images
    jQuery Validation Plugin
    jQuery Stellar Plugin
    jQuery Appear Plugin
    jQuery Animate Number Plugin

Images from
    http://unsplash.com/
    http://gratisography.com
    http://www.graphicsfuel.com/ (Devices MockUp)

---------------------------------------------------------------------
To get the form working please open sendmail.php and change lines:
  | $to      = '<YOUR MAIL HERE>';
  | $subject = '<MESSAGE SUBJECT>';
  | $text    = '<HTML MESSAGE TEXT>';
---------------------------------------------------------------------